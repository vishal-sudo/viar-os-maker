#!/bin/bash
echo
echo "#################################"
echo "Start viar-post"
echo "#################################"

echo "#################################"
echo "          Enable Service         "
echo "#################################"
    systemd_services() {
	local Enable_services=('NetworkManager.service'
                           'bluetooth.service'
                           'fstrim.service'
                           'fstrim.timer'
                           'zramd.service'
                           'cockpit.socket'
                           'nohand.service'
                           'smb.service'
                           'nmb.service'
                           'cups.service'
                           'avahi-daemon.service'
                           'systemd-timesyncd.service'
                           'sddm-plymouth.service')
	local service

	[[ `lspci | grep -i virtualbox` ]] && systemctl enable -f vboxservice.service

	[[ `lspci -k | grep -i qemu` ]] && systemctl enable -f qemu-guest-agent.service

	for service in "${Enable_services[@]}"; do
		systemctl enable -f ${service}
	done
    }

systemd_services

echo "#################################"
echo "      Checking VM Service        "
echo "#################################"
    remove_vbox_pkgs() {
        local vbox_pkg='virtualbox-guest-utils'
        local vboxsrv='/etc/systemd/system/multi-user.target.wants/vboxservice.service'

        lspci | grep -i "virtualbox" >/dev/null
        if [[ "$?" != 0 ]] ; then
            test -n "`pacman -Q $vbox_pkg 2>/dev/null`" && pacman -Rnsdd ${vbox_pkg} --noconfirm
            if [[ -L "$vboxsrv" ]] ; then
                rm -f "$vboxsrv"
            fi
        fi
    }
    remove_vmware_pkgs() {
        local vmware_pkgs=('open-vm-tools' 'xf86-input-vmmouse' 'xf86-video-vmware')
        local _vw_pkg

        lspci | grep -i "VMware" >/dev/null
        if [[ "$?" != 0 ]] ; then
            for _vw_pkg in "${vmware_pkgs[@]}" ; do
			test -n "`pacman -Q ${_vw_pkg} 2>/dev/null`" && pacman -Rnsdd ${_vw_pkg} --noconfirm
            done
        fi
    }
    remove_qemu_pkgs() {
	local qemu_pkg='qemu-guest-agent'
	local qsrvfile='/etc/systemd/system/multi-user.target.wants/qemu-guest-agent.service'

    lspci -k | grep -i "qemu" >/dev/null
    if [[ "$?" != 0 ]] ; then
		echo "+---------------------->>"
		test -n "`pacman -Q $qemu_pkg 2>/dev/null`" && pacman -Rnsdd ${qemu_pkg} --noconfirm
		if [[ -L "$qsrvfile" ]] ; then
			rm -f "$qsrvfile"
		fi
    fi
    }
remove_vbox_pkgs
remove_vmware_pkgs
remove_qemu_pkgs

echo "#################################"
echo "            DETECT GPU           "
echo "#################################"

    detect_gpu() {
       if [[ -n "`lspci -k | grep -P 'VGA|3D|Display' | grep -w "${@}"`" ]]; then
       card_name="${@}"
       fi
    }
detect_gpu 'AMD'
detect_gpu 'Intel Corporation'
detect_gpu 'NVIDIA'

   AMD=no
   NVIDIA=no
   Intel=no

    if [[ "$card_name" == 'AMD' ]] ; then
        AMD=yes
    fi

    if [[ "$card_name" == 'Intel Corporation' ]] ; then
        Intel=yes
    fi

    if [[ "$card_name" == 'NVIDIA' ]] ; then
        NVIDIA=yes
    fi

echo "#################################"
echo "     Remove non-use GPU          "
echo "#################################"

    if [[ "AMD" == 'no' ]] ; then
        _remove_pkgs_if_installed xf86-video-amdgpu xf86-video-ati
    fi

    if [[ "Intel" == 'no' ]] ; then
        _remove_pkgs_if_installed xf86-video-intel
    fi

    if [[ "NVIDIA" == 'no' ]] ; then
        _remove_pkgs_if_installed xf86-video-nouveau nvidia nvidia-settings nvidia-utils
	fi

echo "#################################"
echo "Permissions of important folders"
echo "#################################"
chmod 750 /etc/sudoers.d
chmod 750 /etc/polkit-1/rules.d
chgrp polkitd /etc/polkit-1/rules.d
chmod -v 750 /root

echo "#################################"
echo "Bluetooth improvements"
echo "#################################"
sed -i "s/#AutoEnable=false/AutoEnable=true/g" /etc/bluetooth/main.conf
echo 'load-module module-switch-on-connect' | sudo tee --append /etc/pulse/default.pa

echo "#################################"
echo "Copy /etc/skel to /root"
echo "#################################"
rm -rf /etc/skel/Desktop/*
cp -aT /etc/skel/ /root/

#for i in `ls /home/`; do cp -Rf /etc/skel/. /home/$i/ || exit 0; done
#for i in `ls /home/`; do chown -R $i:$i /home/$i/* || exit 0; done

echo "#################################"
echo "Setting editor to nano"
echo "#################################"
echo "EDITOR=nano" >> /etc/profile


echo "#################################"
echo "Cleanup original files"
echo "#################################"
rm -rf /etc/systemd/system/getty@tty1.service.d
rm -rf /etc/sudoers.d/g_wheel
rm -rf /etc/polkit-1/rules.d/49-nopasswd_global.rules
rm -rf /etc/systemd/system/etc-pacman.d-gnupg.mount
rm -rf /root/{.automated_script.sh,.zlogin}
mv -f /usr/share/tmp/viar-release /etc/lsb-release
mv -f /etc/mkinitcpio.d/viar.linux /etc/mkinitcpio.d/linux.preset
rm -rf /usr/share/tmp
mv -f /usr/local/bin/scripts/pixmap.hook  /etc/pacman.d/hooks/

## os release
# cp -r /etc/tmp/krrishos-release /usr/lib/os-release
# mv -f /etc/tmp/neofetch /usr/bin/neofetch
# cp -r /etc/tmp/krrish-release /usr/bin/lsb-release
# mv -f /etc/arch-release /etc/krrish-release
#mv -f /etc/mkinitcpio.d/krrish /etc/mkinitcpio.d/linux.preset
# ln -s /usr/lib/os-release /etc/os-release
# ln -s /usr/lib/lsb-release /etc/lsb-release

echo "#################################"
echo "End viar-post"
echo "#################################"
rm -rf /usr/local/bin/*
rm -rf /usr/local/viar-welcome/Viar-Tools.py
rm -rf /usr/local/viar-welcome/offline.png
rm -rf /usr/local/viar-welcome/online.png
rm -rf /usr/local/viar-welcome/option.ui
rm -rf /usr/local/viar-welcome/option.py
mv -f /usr/local/viar-welcome/Viar-Tools-post.py /usr/local/viar-welcome/Viar-Tools.py

pacman -Rns viar-scripts
