#! /bin/bash

echo "#######################"
echo "#      Starting       #"
echo "#######################"
echo ""
echo ""
sudo cp -rf ~/.config ~/.config-backup && sudo cp -rf /etc/skel/. ~/
sudo chown 1000:1000 -R ~/

echo ""
echo ""
echo "#######################"
echo "#        Done         #"
echo "#######################"


