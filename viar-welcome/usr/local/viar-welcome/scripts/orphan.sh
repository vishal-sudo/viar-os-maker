#! /bin/bash

echo "#######################"
echo "#      Starting       #"
echo "#######################"
echo ""
echo ""
sudo pacman -Qtdq | sudo pacman -Rns - 2> /dev/null
echo ""
echo ""
echo "#######################"
echo "#        Done         #"
echo "#######################"
