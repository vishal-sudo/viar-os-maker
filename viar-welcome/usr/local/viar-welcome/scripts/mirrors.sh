#! /bin/bash

echo "#######################"
echo "#      Starting       #"
echo "#######################"
echo ""
echo ""
sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist
echo ""
echo ""
echo "#######################"
echo "#        Done         #"
echo "#######################"

