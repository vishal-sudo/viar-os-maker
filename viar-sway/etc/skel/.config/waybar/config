{
    // Draw waybar on the bottom layer.
    "layer": "bottom",
    // Dimensions
    "margin": "4, 4, 0",
    "height": 30,
    "width": 100,

    // Choose the order of the modules
    "modules-left": ["sway/workspaces", "sway/mode", "custom/cmus"],
    "modules-center": ["sway/window"],
    "modules-right": ["idle_inhibitor", "pulseaudio", "backlight", "network", "battery", "clock", "tray"],

    // Modules configuration
    "sway/workspaces": {
        "tooltip": false,
        "disable-scroll": true,
        "max-length": "10"
    },
    "sway/mode": {
        "tooltip": false,
        "format": "<span style=\"italic\">{}</span>"
    },
    "sway/window": {
        "tooltip": false,
	"max-length": 75
    },
    "tray": {
        "tooltip": false,
        "icon-size": 18,
        "spacing": 10
    },
    "clock": {
        "tooltip": false,
        "format-alt": "{:%A, %-d %B}"
    },
    "battery": {
        "tooltip": false,
        "states": {
            "good": 70,
            "warning": 20,
            "critical": 10
        },
        "format": "{capacity}% {icon}",
        "format-full": "{capacity}% ",
        "format-icons": ["", "", "", "", ""]
    },
    "network": {
        "tooltip": false,
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
        "format-disconnected": "Disconnected ⚠"
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "pulseaudio": {
        "tooltip": false,
        "format": "{volume}% {icon}",
        "format-bluetooth": "{volume}% {icon}",
        "format-muted": "",
        "format-icons": {
            "headphones": "",
            "handsfree": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "default": ["", ""]
        },
        "on-click": "pactl set-sink-mute 0 toggle"
    },
    "custom/cmus": {
        "tooltip": false,
        "format": "  <span style=\"italic\">{}</span>",
        "on-click": "cmus-remote -u",
        "on-click-right": "cmus-remote -n",
        "exec": "~/.config/waybar/custom_modules/musicMonitor.sh"
    },
    "backlight": {
        "format": "{percent}% {icon}",
        "format-icons": [""]
    }
}
